package com.toggleetc.greeting;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.ScopeType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;

@RunWith(Arquillian.class)
public class GreeterTest {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Deployment
    public static Archive createDeployment() {
        WebArchive buildedWar = ShrinkWrap.create(WebArchive.class, "testing.war")
                .addClasses(Application.class, Greeting.class, GreetingController.class);

        JavaArchive[] libs = Maven.resolver().loadPomFromFile("pom.xml")
                .importDependencies(ScopeType.PROVIDED, ScopeType.COMPILE)
                .resolve("org.springframework.boot:spring-boot-loader:1.2.3.RELEASE")
                .withTransitivity()
                .as(JavaArchive.class);

        buildedWar.addAsLibraries(libs);

        buildedWar.move("/WEB-INF/lib/spring-boot-starter-tomcat-2.0.5.RELEASE.jar",
                "/WEB-INF/lib-provided/spring-boot-starter-tomcat-2.0.5.RELEASE.jar");
        buildedWar.move("/WEB-INF/lib/tomcat-embed-core-8.5.34.jar",
                "/WEB-INF/lib-provided/tomcat-embed-core-8.5.34.jar");
        buildedWar.move("/WEB-INF/lib/tomcat-embed-el-8.5.34.jar",
                "/WEB-INF/lib-provided/tomcat-embed-el-8.5.34.jar");
        buildedWar.move("/WEB-INF/lib/tomcat-embed-websocket-8.5.34.jar",
                "/WEB-INF/lib-provided/tomcat-embed-websocket-8.5.34.jar");

        return buildedWar;
    }

    @Test
    @RunAsClient
    public void should_create_greeting() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();

        HttpResponse success = client.execute(new HttpGet("http://localhost:8080/testing/greeting"));
        String responseBody = EntityUtils.toString(success.getEntity());
        Greeting greeting = objectMapper.readValue(responseBody, Greeting.class);

        Assert.assertEquals(1, greeting.getId());
        Assert.assertEquals("Hello, World!", greeting.getContent());
        Assert.assertEquals(SC_OK, success.getStatusLine().getStatusCode());

        HttpResponse notFound = client.execute(new HttpGet("http://localhost:8080/testing/incorrect_url"));
        Assert.assertEquals(SC_NOT_FOUND, notFound.getStatusLine().getStatusCode());
    }
}
